namespace Car.Objects
{
    using UnityEngine;

    [CreateAssetMenu(fileName = "New Car", menuName = "Car/Car", order = 0)]
    public class Car : ScriptableObject
    {
        private const float SteerSpeedMAX = 1.0f;
        private const float SteerSpeedMIN = 0.001f;
        private static Car _defaultCar;

        [SerializeField] private float boostForce = 5000.0f;
        [SerializeField] private float brakeForce = 45000.0f;
        [SerializeField] private float downforce = 50.0f;
        [SerializeField] private Drivetrain drivetrain;
        [SerializeField] private Engine engine;
        [SerializeField] private float forwardFriction = 0.5f;
        [SerializeField] private float forwardStiffness = 0.5f;
        [SerializeField] private Gearbox gearbox;
        [SerializeField] private float handbrakeForce = 90000.0f;
        [SerializeField] private float sidewaysFriction = 0.75f;
        [SerializeField] private float sidewaysStiffness = 0.5f;

        [Range(SteerSpeedMIN, SteerSpeedMAX), SerializeField]
        private float steerSpeed = 0.2f;

        public static Car Default
        {
            get
            {
                if (_defaultCar != null) return _defaultCar;
                _defaultCar = CreateInstance<Car>();
                _defaultCar.drivetrain = Drivetrain.Default;
                _defaultCar.engine = Engine.Default;
                _defaultCar.gearbox = Gearbox.Default;
                _defaultCar.name = "Default Car";
                return _defaultCar;
            }
        }

        public float BoostForce => boostForce;
        public float BrakeForce => brakeForce;
        public float Downforce => downforce;
        public Drivetrain Drivetrain => drivetrain;
        public Engine Engine => engine;
        public float ForwardFriction => forwardFriction;
        public float ForwardStiffness => forwardStiffness;
        public Gearbox Gearbox => gearbox;
        public float HandbrakeForce => handbrakeForce;
        public float SidewaysFriction => sidewaysFriction;
        public float SidewaysStiffness => sidewaysStiffness;
        public float SteerSpeed => steerSpeed;
    }
}