namespace Car.Objects
{
    public partial class Drivetrain
    {
        public partial struct Axel
        {
            public enum PowerType : byte
            {
                None = 0,
                Normal = 1,
                Toggle = 2
            }
        }
    }
}