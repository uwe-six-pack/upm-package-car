namespace Car.Behaviours
{
    using System;
    using UnityEngine;

    public partial class CarController
    {
        public partial struct Info
        {
            [Serializable]
            public struct Axel
            {
                [SerializeField] private float steerAngleLeft;
                [SerializeField] private float steerAngleRight;
                [SerializeField] private float track;

                public float SteerAngleLeft => steerAngleLeft;
                public float SteerAngleRight => steerAngleRight;
                public float[] SteerAngles => new[] {steerAngleLeft, steerAngleRight};
                public float Track => track;

                public Axel(Vector3 leftWheel, Vector3 rightWheel)
                {
                    track = Mathf.Abs(leftWheel.x - rightWheel.x);
                    steerAngleLeft = 0.0f;
                    steerAngleRight = 0.0f;
                }

                public Axel(Vector3 leftWheel, Vector3 rightWheel, float turnRadius, float wheelbase)
                {
                    track = Mathf.Abs(leftWheel.x - rightWheel.x);
                    var halfTrack = track / 2.0f;
                    steerAngleLeft = Mathf.Rad2Deg * Mathf.Atan(wheelbase / (turnRadius - halfTrack));
                    steerAngleRight = Mathf.Rad2Deg * Mathf.Atan(wheelbase / (turnRadius + halfTrack));
                }
            }
        }
    }
}