namespace Car.Objects
{
    using UnityEngine;

    [CreateAssetMenu(fileName = "New Drivetrain", menuName = "Car/Drivetrain", order = 0)]
    public partial class Drivetrain : ScriptableObject
    {
        private static Drivetrain _defaultDrivetrain;

        [SerializeField] private Axel[] axels = { };

        public Axel[] Axels => axels;

        public static Drivetrain Default
        {
            get
            {
                if (_defaultDrivetrain != null) return _defaultDrivetrain;
                _defaultDrivetrain = CreateInstance<Drivetrain>();
                _defaultDrivetrain.name = "Default Drivetrain";
                return _defaultDrivetrain;
            }
        }
    }
}