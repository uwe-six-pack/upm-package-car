using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Car.Editor")]
[assembly: InternalsVisibleTo("Car.Tests")]