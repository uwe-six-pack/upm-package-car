namespace Car.Behaviours
{
    using System;
    using UnityEngine;

    public partial class WheelController
    {
        [Serializable]
        public struct Model
        {
            [SerializeField] private Transform @object;
            [SerializeField] private bool position;
            [SerializeField] private Vector3 positionOffset;
            [SerializeField] private bool rotation;
            [SerializeField] private Quaternion rotationOffset;

            public Transform Object { get => @object; set => @object = value; }
            public bool Position { get => position; set => position = value; }
            public Vector3 PositionOffset { get => positionOffset; set => positionOffset = value; }
            public bool Rotation { get => rotation; set => rotation = value; }
            public Quaternion RotationOffset { get => rotationOffset; set => rotationOffset = value; }
        }
    }
}