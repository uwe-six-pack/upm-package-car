namespace Car.Behaviours
{
    using Objects;
    using UnityEngine;

    [AddComponentMenu("Car/Car Controller"),
     DisallowMultipleComponent,
     RequireComponent(typeof(Rigidbody))]
    public partial class CarController : MonoBehaviour
    {
        [SerializeField] private Car car;
        [SerializeField] private Transform centerOfMass;
        [SerializeField] private Info info = Info.Default;
        [SerializeField] private Input input = Input.Default;
        [SerializeField] private WheelController[] wheels;

        private Rigidbody _rigidbody;

        public Car Car => car;

        public void Start()
        {
            _rigidbody = GetComponent<Rigidbody>();
            if (car == null) car = Car.Default;

            if (centerOfMass != null) _rigidbody.centerOfMass = centerOfMass.localPosition;

            info = new Info(car, wheels);

            foreach (var wheel in wheels)
            {
                wheel.ForwardFriction = car.ForwardFriction;
                wheel.SidewaysFriction = car.SidewaysFriction;
            }
        }

        public void FixedUpdate()
        {
            #region Setup

            var drivetrain = car.Drivetrain;
            var engine = car.Engine;
            var gearbox = car.Gearbox;
            var velocity = _rigidbody.velocity.magnitude;
            info.Speed = velocity * 3.6f;
            _rigidbody.AddForce(-transform.up * (car.Downforce * velocity));

            #endregion

            #region Engine

            var wheelRpmSum = 0.0f;
            for (var i = 0; i < drivetrain.Axels.Length; i++)
            {
                for (var j = 0; j < 2; j++)
                {
                    if (drivetrain.Axels[i].IsPoweredNormal) wheelRpmSum += wheels[(i * 2) + j].RPM;
                }
            }

            info.WheelRPM = info.NormalWheelCount == 0 ? 0.0f : wheelRpmSum / info.NormalWheelCount;

            info.Torque = engine.TorqueAt(Mathf.Max(engine.Idle, info.EngineRPM)) * gearbox.RatioAt(info.Gear);
            var tmp = 0.0f;
            info.EngineRPM = Mathf.SmoothDamp(info.EngineRPM,
                engine.Idle + Mathf.Abs(info.WheelRPM) * gearbox.RatioAt(info.Gear),
                ref tmp, 0.01f);

            #endregion

            #region Gearbox

            if (info.Gear == -1 && info.EngineRPM <= engine.Idle * 1.1f)
            {
                info.Gear = 0;
            }

            if (info.Gear == 0 && info.EngineRPM <= engine.Idle * 1.1f && input.Brake > 0.0f)
            {
                info.Gear = -1;
            }
            /*else
            {
                info.Gear = info.Gear == -1 ? 0 : info.Gear;
            }*/

            if (info.EngineRPM < engine.ShiftDown && info.Gear > 0 && info.Gear <= gearbox.Gears.Length - 1)
            {
                info.Gear--;
            }

            if (info.EngineRPM > engine.ShiftUp && info.Gear >= 0 && info.Gear < gearbox.Gears.Length - 1)
            {
                info.Gear++;
            }

            #endregion

            #region Input

            var accelerate = input.Accelerate;
            var brake = input.Brake;

            if (info.Gear == -1)
            {
                accelerate = -input.Brake;
                brake = input.Accelerate;
            }

            #endregion

            #region Wheels

            for (var i = 0; i < drivetrain.Axels.Length; i++)
            {
                var axel = drivetrain.Axels[i];
                var axelInfo = info.Axels[i];
                var left = 0.0f;
                var right = 0.0f;
                if (input.Steer < 0.0f)
                {
                    left = axelInfo.SteerAngleLeft;
                    right = axelInfo.SteerAngleRight;
                }

                if (input.Steer > 0.0f)
                {
                    left = axelInfo.SteerAngleRight;
                    right = axelInfo.SteerAngleLeft;
                }

                for (var j = 0; j < 2; j++)
                {
                    var wheel = wheels[(i * 2) + j];
                    // Torque
                    if (axel.IsPoweredNormal) wheel.MotorTorque = (info.Torque * accelerate) / info.NormalWheelCount;

                    // Brake
                    wheel.BrakeTorque = 0.0f;
                    if (axel.Brake)
                        wheel.BrakeTorque += (brake * car.BrakeForce);
                    if (axel.IsHandbrake)
                    {
                        wheel.BrakeTorque += (input.Handbrake * car.HandbrakeForce);
                        if (input.Handbrake > 0.0f)
                        {
                            wheel.ForwardStiffness = Mathf.SmoothDamp(wheel.ForwardStiffness, car.ForwardStiffness,
                                ref velocity, Time.fixedDeltaTime);
                            wheel.SidewaysStiffness = Mathf.SmoothDamp(wheel.SidewaysStiffness, car.SidewaysStiffness,
                                ref velocity, Time.fixedDeltaTime);
                        }
                    }

                    // Steer
                    if (axel.IsSteerNormal)
                        wheel.SteerAngle = Mathf.LerpAngle(wheel.SteerAngle, input.Steer * (j == 0 ? left : right),
                            car.SteerSpeed);
                    if (axel.IsSteerInverted)
                        wheel.SteerAngle = Mathf.LerpAngle(wheel.SteerAngle, input.Steer * -(j == 0 ? left : right),
                            car.SteerSpeed);
                }

                if (input.Boost) _rigidbody.AddForce(transform.forward * car.BoostForce);
            }

            #endregion
        }

        public void Restart()
        {
            info = new Info(car, wheels);
            input = Input.Default;
            _rigidbody.angularVelocity = Vector3.zero;
            _rigidbody.velocity = Vector3.zero;
            foreach (var wheel in wheels)
            {
                wheel.Restart();
            }
        }
    }
}