namespace Car.Objects
{
    using UnityEngine;

    [CreateAssetMenu(fileName = "New Engine", menuName = "Car/Engine", order = 0)]
    public class Engine : ScriptableObject
    {
        private static Engine _defaultEngine;

        [SerializeField] private float idle = 1000.0f;
        [SerializeField] private float redline = 6000.0f;
        [SerializeField] private float shiftDown = 1500.0f;
        [SerializeField] private float shiftUp = 5500.0f;

        [SerializeField] private AnimationCurve torque = new AnimationCurve(new Keyframe(0.0f, 100.0f),
            new Keyframe(4500.0f, 900.0f), new Keyframe(6000.0f, 75.0f));

        public static Engine Default
        {
            get
            {
                if (_defaultEngine != null) return _defaultEngine;
                _defaultEngine = CreateInstance<Engine>();
                _defaultEngine.name = "Default Engine";
                return _defaultEngine;
            }
        }

        public float Idle => idle;
        public float Redline => redline;
        public float ShiftDown => shiftDown;

        public float ShiftUp => shiftUp;

        public AnimationCurve Torque => torque;

        public float TorqueAt(float rpm)
        {
            if (rpm < idle || rpm > redline) return 0.0f;
            return torque.Evaluate(rpm);
        }
    }
}