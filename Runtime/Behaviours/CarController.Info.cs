namespace Car.Behaviours
{
    using Objects;
    using System;
    using System.Linq;
    using UnityEngine;

    public partial class CarController
    {
        public float EngineRPM => info.EngineRPM;
        public int Gear => info.Gear;
        public float Speed => info.Speed;
        public float Torque => info.Torque;
        public float WheelRPM => info.WheelRPM;

        [Serializable]
        public partial struct Info
        {
            private static readonly Info DefaultInfo =
                new Info
                {
                    axels = new Axel[] { },
                    engineRPM = 0.0f,
                    gear = 0,
                    normalWheelCount = 0,
                    speed = 0.0f,
                    wheelbase = 0.0f,
                    wheelRPM = 0.0f
                };

            [SerializeField] private Axel[] axels;
            [SerializeField] private float engineRPM;
            [SerializeField] private int gear;
            [SerializeField] private int normalWheelCount;
            [SerializeField] private float speed;
            [SerializeField] private float torque;
            [SerializeField] private float wheelbase;
            [SerializeField] private float wheelRPM;

            public Info(Car car, WheelController[] wheels)
            {
                var drivetrain = car.Drivetrain;

                switch (wheels.Length)
                {
                    case 0:
                    case 1:
                        wheelbase = 0.0f;
                        break;
                    default:
                        wheelbase = Mathf.Abs(wheels.First().transform.position.z - wheels.Last().transform.position.z);
                        break;
                }

                axels = new Axel[drivetrain.Axels.Length];
                for (var i = 0; i < drivetrain.Axels.Length; i++)
                {
                    var axel = drivetrain.Axels[i];
                    if (axel.IsSteer)
                    {
                        axels[i] = new Axel(wheels[i * 2].transform.position, wheels[(i * 2) + 1].transform.position,
                            axel.TurnRadius, wheelbase);
                    }
                    else
                    {
                        axels[i] = new Axel(wheels[i * 2].transform.position, wheels[(i * 2) + 1].transform.position);
                    }
                }

                engineRPM = 0.0f;
                gear = 0;
                normalWheelCount = drivetrain.Axels.Count(axel => axel.IsPoweredNormal) * 2;
                speed = 0.0f;
                torque = 0.0f;
                wheelRPM = 0.0f;
            }

            public Axel[] Axels => axels;
            public static Info Default => DefaultInfo;
            public float EngineRPM { get => engineRPM; set => engineRPM = value; }
            public int Gear { get => gear; set => gear = value; }
            public int NormalWheelCount => normalWheelCount;
            public float Speed { get => speed; set => speed = value; }
            public float Torque { get => torque; set => torque = value; }
            public float Wheelbase => wheelbase;
            public float WheelRPM { get => wheelRPM; set => wheelRPM = value; }
        }
    }
}