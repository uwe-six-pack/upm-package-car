namespace Car.Objects
{
    public partial class Drivetrain
    {
        public partial struct Axel
        {
            public enum SteerType : byte
            {
                Inverted = 2,
                None = 0,
                Normal = 1
            }
        }
    }
}