namespace Car.Behaviours
{
    using System;
    using UnityEngine;

    [AddComponentMenu("Car/Wheel Controller"),
     DisallowMultipleComponent,
     RequireComponent(typeof(WheelCollider))]
    public partial class WheelController : MonoBehaviour
    {
        [SerializeField] private Model model = new Model
        {
            Object = null,
            Position = true,
            PositionOffset = Vector3.zero,
            Rotation = true,
            RotationOffset = Quaternion.identity
        };

        private WheelCollider _collider;

        public float BrakeTorque { get => _collider.brakeTorque; set => _collider.brakeTorque = value; }

        public float ForwardFriction
        {
            get => _collider.forwardFriction.asymptoteValue;
            set
            {
                var curve = _collider.forwardFriction;
                curve.asymptoteValue = value;
                _collider.forwardFriction = curve;
            }
        }

        public float ForwardStiffness
        {
            get => _collider.forwardFriction.stiffness;
            set
            {
                var curve = _collider.forwardFriction;
                curve.stiffness = value;
                _collider.forwardFriction = curve;
            }
        }

        public float MotorTorque { get => _collider.motorTorque; set => _collider.motorTorque = value; }
        public float RPM => _collider.rpm;

        public float SidewaysFriction
        {
            get => _collider.sidewaysFriction.asymptoteValue;
            set
            {
                var curve = _collider.sidewaysFriction;
                curve.asymptoteValue = value;
                _collider.sidewaysFriction = curve;
            }
        }

        public float SidewaysStiffness
        {
            get => _collider.sidewaysFriction.stiffness;
            set
            {
                var curve = _collider.sidewaysFriction;
                curve.stiffness = value;
                _collider.sidewaysFriction = curve;
            }
        }

        public float SteerAngle { get => _collider.steerAngle; set => _collider.steerAngle = value; }

        public void Start()
        {
            _collider = GetComponent<WheelCollider>();

            _collider.motorTorque = float.Epsilon;
        }

        public void FixedUpdate()
        {
            _collider.GetWorldPose(out Vector3 position, out Quaternion rotation);
            if (model.Position) model.Object.position = position + model.PositionOffset;
            if (model.Rotation) model.Object.rotation = rotation * model.RotationOffset;
        }

        public void Restart()
        {
            _collider.brakeTorque = 0.0f;
            _collider.motorTorque = float.Epsilon;
            _collider.steerAngle = 0.0f;
        }
    }
}