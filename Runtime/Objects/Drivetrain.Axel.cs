namespace Car.Objects
{
    using System;
    using UnityEngine;

    public partial class Drivetrain
    {
        [Serializable]
        public partial struct Axel
        {
            [SerializeField] private bool brake;
            [SerializeField] private bool handbrake;
            [SerializeField] private PowerType power;
            [SerializeField] private SteerType steer;
            [SerializeField] private float turnRadius;

            public bool Brake { get => brake; set => brake = value; }
            public bool Handbrake { get => handbrake; set => handbrake = value; }
            public bool IsBrake => brake;
            public bool IsHandbrake => handbrake;
            public bool IsPowered => power == PowerType.Normal || power == PowerType.Toggle;
            public bool IsPoweredNormal => power == PowerType.Normal;
            public bool IsPoweredToggle => power == PowerType.Toggle;
            public bool IsSteer => steer == SteerType.Inverted || steer == SteerType.Normal;
            public bool IsSteerInverted => steer == SteerType.Inverted;
            public bool IsSteerNormal => steer == SteerType.Normal;
            public PowerType Power { get => power; set => power = value; }
            public SteerType Steer { get => steer; set => steer = value; }
            public float TurnRadius { get => turnRadius; set => turnRadius = value; }
        }
    }
}