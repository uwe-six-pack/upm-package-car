namespace Car.Behaviours
{
    using System;
    using UnityEngine;

    public partial class CarController
    {
        public float Accelerate { get => input.Accelerate; set => input.Accelerate = value; }
        public bool AllWheelDrive { get => input.AllWheelDrive; set => input.AllWheelDrive = value; }
        public bool Boost { get => input.Boost; set => input.Boost = value; }
        public float Brake { get => input.Brake; set => input.Brake = value; }
        public float Handbrake { get => input.Handbrake; set => input.Handbrake = value; }
        public float Steer { get => input.Steer; set => input.Steer = value; }

        [Serializable]
        public struct Input
        {
            private static readonly Input DefaultInput = new Input
            {
                accelerate = 0.0f,
                allWheelDrive = false,
                boost = false,
                brake = 0.0f,
                handbrake = 0.0f,
                steer = 0.0f
            };

            [Range(0.0f, 1.0f), SerializeField] private float accelerate;
            [SerializeField] private bool allWheelDrive;
            [SerializeField] private bool boost;
            [Range(0.0f, 1.0f), SerializeField] private float brake;
            [Range(0.0f, 1.0f), SerializeField] private float handbrake;
            [Range(-1.0f, 1.0f), SerializeField] private float steer;

            public float Accelerate { get => accelerate; set => accelerate = Mathf.Clamp01(value); }
            public bool AllWheelDrive { get => allWheelDrive; set => allWheelDrive = value; }
            public bool Boost { get => boost; set => boost = value; }
            public float Brake { get => brake; set => brake = Mathf.Clamp01(value); }
            public static Input Default => DefaultInput;
            public float Handbrake { get => handbrake; set => handbrake = Mathf.Clamp01(value); }
            public float Steer { get => steer; set => steer = Mathf.Clamp(value, -1.0f, 1.0f); }
        }
    }
}