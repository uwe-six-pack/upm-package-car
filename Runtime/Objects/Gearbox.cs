namespace Car.Objects
{
    using UnityEngine;

    [CreateAssetMenu(fileName = "New Gearbox", menuName = "Car/Gearbox", order = 0)]
    public class Gearbox : ScriptableObject
    {
        private static Gearbox _defaultGearbox;

        [SerializeField] private float finalDrive = 1.0f;
        [SerializeField] private float[] gears = { };
        [SerializeField] private float reverse = 1.0f;

        public static Gearbox Default
        {
            get
            {
                if (_defaultGearbox != null) return _defaultGearbox;
                _defaultGearbox = CreateInstance<Gearbox>();
                _defaultGearbox.gears = new[] {1.0f};
                _defaultGearbox.name = "Default Gearbox";
                return _defaultGearbox;
            }
        }

        public float FinalDrive => finalDrive;
        public float[] Gears => gears;
        public float Reverse => reverse;

        public float RatioAt(int gear)
        {
            if (gear == -1) return finalDrive * reverse;
            return finalDrive * gears[gear];
        }
    }
}