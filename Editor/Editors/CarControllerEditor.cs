namespace Car.Editor.Editors
{
    using Behaviours;
    using Objects;
    using UnityEditor;
    using UnityEditor.UIElements;
    using UnityEngine;
    using UnityEngine.UIElements;

    [CanEditMultipleObjects,
     CustomEditor(typeof(CarController))]
    public class CarControllerEditor : Editor
    {
        private VisualTreeAsset _axelFieldTemplate;
        private PropertyField _carField;
        private VisualElement _root;
        private SerializedProperty _wheels;
        private Foldout _wheelsFoldout;
        private SerializedProperty _wheelsSize;

        public override VisualElement CreateInspectorGUI()
        {
            Debug.Log("CreateInspectorGUI");
            return _root;
        }

        private void OnCarFieldChanged(ChangeEvent<Object> evt)
        {
            UpdateWheels(evt.newValue == null ? 0 : ((Car)evt.newValue).Drivetrain.Axels.Length * 2);
        }

        private void OnEnable()
        {
            Debug.Log("OnEnable");

            // Load data
            _axelFieldTemplate = Resources.Load<VisualTreeAsset>("Layouts/Fields/AxelField");
            var visualTreeAsset = Resources.Load<VisualTreeAsset>("Layouts/Editors/CarControllerEditor");
            _root = visualTreeAsset.CloneTree();
            _root.styleSheets.Add(EditorGUIUtility.isProSkin
                ? Resources.Load<StyleSheet>("Styles/CommonDark")
                : Resources.Load<StyleSheet>("Styles/CommonLight"));
            _wheels = serializedObject.FindProperty("wheels");
            _wheelsSize = _wheels.FindPropertyRelative("Array.size");

            // Query the elements
            _carField = _root.Q<PropertyField>("car-field");
            _carField.RegisterCallback<ChangeEvent<Object>>(OnCarFieldChanged);
            _root.Q<PropertyField>("info-field").SetEnabled(false);
            _root.Q<PropertyField>("input-field").SetEnabled(false);
            _wheelsFoldout = _root.Q<Foldout>("wheels-foldout");

            // Update serializedObject if necessary
            var car = serializedObject.FindProperty("car").objectReferenceValue;
            UpdateWheels(car == null ? 0 : ((Car)car).Drivetrain.Axels.Length * 2);
            UpdateWheelsUI();
        }

        private void UpdateWheels(int wheelCount)
        {
            serializedObject.UpdateIfRequiredOrScript();
            if (_wheelsSize.intValue == wheelCount) return;
            _wheelsSize.intValue = wheelCount;
            serializedObject.ApplyModifiedPropertiesWithoutUndo();
            UpdateWheelsUI();
        }

        private void UpdateWheelsUI()
        {
            _wheelsFoldout.Clear();
            for (var i = 0; i < _wheels.arraySize / 2; i++)
            {
                var axel = _axelFieldTemplate.CloneTree();
                var lInput = axel.Q<ObjectField>("car-l-input");
                lInput.bindingPath = _wheels.GetArrayElementAtIndex((i * 2) + 0).propertyPath;
                lInput.objectType = typeof(WheelController);
                var rInput = axel.Q<ObjectField>("car-r-input");
                rInput.bindingPath = _wheels.GetArrayElementAtIndex((i * 2) + 1).propertyPath;
                rInput.objectType = typeof(WheelController);
                axel.Q<Label>("car-label").text = $"Axel {i}";
                _wheelsFoldout.Add(axel);
            }
        }
    }
}